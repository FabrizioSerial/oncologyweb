import React from 'react'
import '../buttonsHome/ButtonHome.css'


export const ButtonHome = (props) => {
    return(
            props.color == "purple" ? 
            <button className="purple-button">
                <p>
                    {props.text}
                </p>
            </button>: props.color == "blue" ?
            <button className="blue-button">
                <p>{props.text}</p>
            </button> : 
            <button className="lightblue-button">
                <p>{props.text}</p>
            </button>
    )
}